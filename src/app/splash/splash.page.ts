import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  constructor(private storage: Storage, private router: Router) { }
  async finish() {
    await this.storage.set('tutorialComplete', true);
    this.router.navigateByUrl('/');
  }
  ngOnInit() {
  }

}

